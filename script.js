

var app=angular.module('plunker', ['ngRoute']);

app.directive("groceryListDirective",function(){
  return{
    restrict:"E",
    templateUrl:"groceryDirective.html"
  }
});
app.config(function($routeProvider, $locationProvider) {
  $routeProvider
    
    .when("/addItem", {
      templateUrl : "addItem.html",
      controller:"addItem"
  })
  .when("/addItem/:id", {
    templateUrl : "addItem.html",
    controller:"addItem"
})
.when("/", {
  templateUrl : "groceryList.html",
  controller:"listItems"
})
  .otherwise({
    redirectTo:"/"
  })
});
app.service("Groceryservice",function(){
  var groceryService={};
  groceryService.groceryList=[{
    id:1,itemName:"Milk",date:"2018-09-21"
  },
  {
    id:2,itemName:"Suger",date:"2018-01-01"
  },
  {
    id:3,itemName:"Bread",date:"2018-05-21"
  },
  {
    id:4,itemName:"Oil",date:"2017-04-17"
  },{
    id:5,itemName:"Egg",date:"2018-06-30"
  }
  ];
  groceryService.save= function (newGroceryItem){
    groceryService.groceryList.push(newGroceryItem);
  }
  groceryService.update= function (newGroceryItem){
    var index = groceryService.groceryList.map(e=>e.id).indexOf(newGroceryItem.id);
    groceryService.groceryList[index] = newGroceryItem;
  }
  return groceryService;

});
app.controller('MainCtrl', function($scope) {
  $scope.appTitle = 'Grocery List';
});
app.controller('addItem',function($scope,$routeParams,Groceryservice,$location){
  $scope.title="Add item Below";
  $scope.paramValue=$routeParams.id;
  if($routeParams.id !== undefined){

    var gItems = Groceryservice.groceryList;
    var index = gItems.map(e=>e.id).indexOf(parseInt($routeParams.id));
    $scope.item = gItems[index];
  }

  $scope.save=function(){
    var gItems = Groceryservice.groceryList;
    var id = "";

    if($routeParams.id !== undefined){
      id = $routeParams.id;
      groceryItem={id:id,itemName:$scope.item.itemName,date:new Date()};
      Groceryservice.update(groceryItem);
    }else{
      var allIds = gItems.map(e=>e.id);
      var maxId = Math.max(...allIds);
      id = maxId +1;
      groceryItem={id:id,itemName:$scope.item.itemName,date:new Date()};
      Groceryservice.save(groceryItem);
    }
    $location.path("/");
  }

});
app.controller("listItems",function($scope,Groceryservice){
  
  $scope.groceryList=Groceryservice.groceryList;
});